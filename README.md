### Features
* Facebook Comments


### Installation

``` 
npm install --save vue-facebook
```

## Usage

```js
// src/main.js
import Vue from 'vue';
import VueFacebook from 'vue-facebook';

Vue.use(VueFacebook)
```
or
```html
<script src="https://unpkg.com/vue"></script>
<script src="https://unpkg.com/vue-facebook"></script>
<script>
new Vue(
	{
		el: '#app'
	}
)
</script>
```

In Vue/HTML template

```vue
<template>
    <div class="content">
        <div class="post">
            <!-- Article -->
        </div>
        <fb-comment url="your-url.com/post-slug" facebookappid="{FACEBOOK APP ID}" />
    </div>
</template>
```